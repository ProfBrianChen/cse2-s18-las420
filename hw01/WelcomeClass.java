////////////////////////////
///CSE 2 Welcome Class
///

public class WelcomeClass {
  
    public static void main(String[] args) {
      System.out.println("  -----------");
      System.out.println("  | WELCOME |");
      System.out.println("  -----------");
      System.out.println("  ^  ^  ^  ^  ^  ^");
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<-L--A--S--4--2--0->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");
      System.out.println("My name is Lindsay Slavit and I am a sophomore in the IBE Program at Lehigh University");
    }
}