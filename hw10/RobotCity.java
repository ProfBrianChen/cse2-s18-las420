///////////////////////////
///Lindsay Slavit 4.24.18
///CSE 2 --> Create a program 


import java.util.Scanner;

public class RobotCity {
  
  public static int[][] buildCity() {
    int eastWest= 10+ (int)(Math.random()*6);
    int northSouth=10+(int)(Math.random()*6);
    int[][]city= new int[eastWest][northSouth];
    
    for(int i=0; i<eastWest; i++) {
      for(int j=0; j<northSouth; j++) {
        city[i][j]= 100+ (int)(Math.random()*900);
      }
    }
    return city;
  }
  
  public static void display(int[][] city) {
    for(int i=0; i<city.length; i++) {
      for(int j=0; j<city[i].length; j++) {
        System.out.printf("%5d",city[i][j]);
      }
      System.out.println();
    }
  }
  
  public static int[][] invade(int[][] city, int k) {
    int eastWest=0;
    int northSouth=0;
    for (int i=0; i<k; i++) {
      do {
      eastWest=(int)(Math.random()*city.length);
      northSouth=(int)(Math.random()*city[eastWest].length);
      } while (city[eastWest][northSouth]<0);
      city[eastWest][northSouth] *= -1; 
    }
    return city;
  }
  
  public static int[][] update(int[][] city) {
    int[][] temp=new int[city.length][city[0].length];
    for(int i=0; i<city.length; i++) {
      for(int j=0; j<city[i].length; j++) {
        temp[i][j]=city[i][j]; 
      }
    }
    for(int i=0; i<city.length; i++) {
      for(int j=0; j<city[i].length; j++) {
        if (city[i][j]<0) {
         temp[i][j]=city[i][j]*-1; 
         if(j!=city[i].length-1) {
            temp[i][j+1] =city[i][j+1]* -1;
         }
        }
      }
    }
    return temp;
  }
  
  public static void main (String[]args) {
    Scanner A=new Scanner(System.in);
  
    for (int i=0; i<5; i++) {  
      int[][] city=buildCity(); //build city
      System.out.println();
      display(city); //display city
      System.out.print("Enter the number of robots: "); //prompt user
      int k=A.nextInt();
      city= invade(city,k); 
      System.out.println();
      display(city);//display city
      city= update(city); //update robots
      System.out.println();
      display(city); //display city
   }  
  }
  
  
}