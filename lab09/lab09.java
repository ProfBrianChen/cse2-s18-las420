////////////////////////////////////////////////
///Lindsay Slavit 4.13.18
///CSE 2

public class lab09 {
  
  public static int[] copy(int[] array) {
    
    int[] array1= new int[array.length];
    for (int i=0; i<array.length; i++) {
      array1[i]=array[i];
    }
    return array1;
  }
  
  public static void inverter(int[] array) {
    int[] temp=new int[array.length];
    int j=temp.length-1;
    for (int i=0; i<8; i++) {
      temp[i]=array[i];
    }
    for (int i=0; i<j; i++) {
      array[i]=temp[j-i];
    }
  }
  
  public static int[] inverter2(int[] array) {
    int[] arrayCopy=copy(array);
    int[] temp=new int[arrayCopy.length];
    for (int i=0; i<arrayCopy.length; i++) {
      temp[i]=arrayCopy[i];
    }
    for (int i=0; i<array.length; i++) {
      arrayCopy[i]=temp[arrayCopy.length-1-i];
    }
    return arrayCopy;
  }
  
  public static void print(int[] array) {
    for (int i=0; i<array.length; i++) {
      System.out.print(array[i] +" ");
    }
    System.out.println();
  }
  
  public static void main(String[]args) {
    int[] array0={1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1=copy(array0); //copy array
    int[] array2=copy(array0); //copy array
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3=inverter2(array2);
    print(array3);
  }
  
}