///////////////////////////////////////////////////////////
///Lindsay Slavit 4.6.18
///CSE 2 --> Create a program that will print students' names and grades using an array

import java.util.Scanner;
import java.util.Random;

public class lab08 {
  
  public static void main(String[]args) {
    Scanner A=new Scanner (System.in);
    Random R= new Random();
    String[] studentNames;
    int studentNumber= R.nextInt(6)+5; //generate a random number between 5 and 10 (inclusive)
    studentNames= new String[studentNumber];
    String name="a";
    int i=0; //initialize counter
    for (i=0; i<studentNumber; i++) {
      System.out.print("Enter " +studentNumber+ " Student Names: "); //prompt user input
      studentNames[i]=A.next();
      System.out.println(studentNames[i]);
    }
    int[] midterm;
    midterm=new int[studentNumber];
    System.out.println("Here are the students and their grades:");
    for (int j=0; j<studentNumber; j++) {
      midterm[j]= (int)(Math.random()*100+ 1); //generate grade
      System.out.println("Student: " +studentNames[j]+ "   Grade: " +midterm[j]);
      
    }
  }
  
}