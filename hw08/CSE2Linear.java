///////////////////////////
///Lindsay Slavit 4.10.18
///CSE 2 --> Create a program that lets the user input grades, searches for a particular one, and then randomizes the grades

import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
  
  public static int binarySearch (int studentGrades[], int target){
		int start=0;
		int end=studentGrades.length-1;
    int half=start+(end-1)/2;
		while(start<end) {
    if (studentGrades[half]==target){
      return studentGrades[half];
    }
    else if (studentGrades[half]>target) {
			end=half-1;
    }
    else if (studentGrades[half]<target) {
			start=half+1;
    } 
  } return -1;
	}
  public static int[] scrambled(int studentGrades[]) {
    for (int i=0; i<studentGrades.length; i++) {
      int target = (int) (studentGrades.length * Math.random() );
	    int temp = studentGrades[target];
    	studentGrades[target] = studentGrades[i];
	    studentGrades[i] = temp;
    }
    return studentGrades;
  }
  
  public static int linearSearch(int studentGrades[], int goal) {
    for (int i=0; i<studentGrades.length; i++) {
      if (i==goal) {
        return i;
      }
    }
    return -1;
  }
    
  
  
  public static void main (String[] args) {
    Scanner A=new Scanner(System.in);
    Random R=new Random();
    
    int[] studentGrades; //declare array
    studentGrades=new int[15];
    System.out.print("Enter 15 ascending ints for final grades in CSE2: "); //prompt user 
    for (int i=0; i<15; i++) {
      
      
      boolean flag=A.hasNextInt(); //check if input is int
      while (flag==false) {
        System.out.print("Please enter an integer: ");
        studentGrades[i]=A.nextInt();
        flag=A.hasNextInt();
      }
      studentGrades[i]=A.nextInt(); //assign grades to students
      if (i>1 && studentGrades[i]<studentGrades[i-1]) {
        do {
          System.out.print("Please enter an integer larger than the previous one: ");
          studentGrades[i]=A.nextInt();
      } while (studentGrades[i]<studentGrades[i-1]);
    }
      if(studentGrades[i]>100 || studentGrades[i]<0) {
        do {
          System.out.print("Please enter an integer in the range 0-100: ");
          studentGrades[i]=A.nextInt();
        } while (studentGrades[i]>100 || studentGrades[i]<0); 
      }}
      
      for (int i=0; i<15; i++) {
        System.out.print(studentGrades[i]+" ");
       }
      System.out.println();
			System.out.print("Enter a grade to search for: ");
      while (A.hasNextInt()==false) {
				A.next();
        System.out.print("Please enter an integer: ");
			}
      int target=A.nextInt();
      int result=binarySearch(studentGrades, target);
      if (result>0) {
        System.out.println(target+ " was found!");
      }
      if (result<0) {
        System.out.println(target+ " was not found.");
      }
      System.out.println("Scrambled: ");
      int[] newStudentGrades=scrambled(studentGrades);
      for (int i=0;i<15;i++)
       {System.out.print(newStudentGrades[i]);
       }
      System.out.println();
      System.out.println("Enter a grade to search for: ");
      int target2=A.nextInt();
      int linearResult=linearSearch(newStudentGrades, target2);
      if (linearResult>0) {
        System.out.print(target2+ " was found!");
      }
      else {
        System.out.print(target2+ " was not found");
      }
      }
    
    
    
  }

