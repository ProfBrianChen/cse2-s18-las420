///////////////////////////////////////////////////////////
///Lindsay Slavit 3.27.18
///CSE 2 --> Create a program that will print the area of a rectange, circle, or triangle

import java.util.Scanner;

public class Area {
  
  public static double rectArea (double width, double rectHeight, Scanner A) {
    System.out.print("Please enter the width of the rectangle: "); //prompt user to enter width
    boolean flag1=A.hasNextDouble(); //confirm input is of type double
    width=A.nextDouble();
    while (flag1==false) {
      String junkWord = A.next();      
      flag1= A.hasNextDouble();
      System.out.print("Please enter a number for the width of the rectangle: "); //prompt user to enter width
      width=A.nextDouble();
    }
    System.out.print("Please enter the height of the rectangle: "); //prompt user to enter height
    boolean flag2=A.hasNextDouble();
    rectHeight=A.nextDouble();
    while (flag2==false) {
      String junkWord = A.next();      
      flag2= A.hasNextDouble();
      System.out.print("Please enter a number for the height of the rectangle: "); //prompt user to enter width
      rectHeight=A.nextDouble();
    }
    double area=width*rectHeight; //calculate rectangle area
    return area;
    
  }
  
   public static double circleArea (double diameter, Scanner A) {
    System.out.print("Please enter the diameter of the circle: "); //prompt user to enter width
    boolean flag1=A.hasNextDouble(); //confirm input is of type double
    diameter=A.nextDouble();
    while (flag1==false) {
      String junkWord = A.next();      
      flag1= A.hasNextDouble();
      System.out.print("Please enter a number for the diameter of the circle: "); //prompt user to enter width
      diameter=A.nextDouble();
    }
    double radiusSquared=Math.pow(diameter/2,2); //calculate the radius squared
    double area=Math.PI*radiusSquared; //calculate circle area
    return area;
    
  }
  
   public static double triangleArea (double base, double triHeight, Scanner A) {
    System.out.print("Please enter the base of the triangle: "); //prompt user to enter width
    boolean flag1=A.hasNextDouble(); //confirm input is of type double
    base=A.nextDouble();
    while (flag1==false) {
      String junkWord = A.next();      
      flag1= A.hasNextDouble();
      System.out.print("Please enter a number for the base of the triangle: "); //prompt user to enter width
      base=A.nextDouble();
    }
    System.out.print("Please enter the height of the triangle: "); //prompt user to enter height
    boolean flag2=A.hasNextDouble();
    triHeight=A.nextDouble();
    while (flag2==false) {
      String junkWord = A.next();      
      flag2= A.hasNextDouble();
      System.out.print("Please enter a number for the height of the triangle: "); //prompt user to enter width
      triHeight=A.nextDouble();
    }
    double area=.5*base*triHeight; //calculate triangle area
    return area;
    
    
  }
 
  public static void main (String[] args) {
    
    Scanner A= new Scanner (System.in);
    System.out.print("Please enter a shape: rectange, triangle, circle- "); //prompt user to enter shape
    boolean flag=A.hasNext();
    String shape=A.next();
    double area=0;
    double width=0;
    double rectHeight=0;
    double diameter=0;
    double base=0;
    double triHeight=0;
    while (flag==false) {
      System.out.print("Please enter rectangle, triangle, or circle");
      flag=A.hasNext();
      shape=A.next();
    }
    if (shape.equals("rectangle")) {
      area= rectArea ( width,rectHeight,A); //run rectangle method
    }
    else if (shape.equals("circle")) {
      area= circleArea (diameter,A); //run circle method
    }
    else if (shape.equals("triangle")) {
      area= triangleArea (base, triHeight,A); //run triangle method
    }
    else {
      System.out.print("Incorrect shape: Please enter rectangle, triange, or circle");
    }
    System.out.printf("The area is %.2f: ",area);
     
  }
    
}
    
    
    