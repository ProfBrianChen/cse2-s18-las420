///////////////////////////////////////////////////////////
///Lindsay Slavit 3.27.18
///CSE 2 --> Create a program that will analyze all characters in a string

import java.util.Scanner;

public class StringAnalysis {
  
  
  public static boolean wholeWord (String word, Scanner A) {
    int length = String.valueOf(word).length(); //check the length of the word
    int k=0;
    boolean value=false;
    char test='0';
    for (int i=0; i<length-1; i++) { //test all chars in the string
      test=word.charAt(i);
      if ('a'<=test && test<='z') {
        k=k+1; //increment k to check if all characters are letters
      }
    }
    if (length==1+k) {
      value= true;
    }
    else {
      value= false; 
    }
    return value;
  }
  
  public static boolean partWord (String word, int amount, Scanner A) {
    int k=0;
    char test='0';
    boolean value=false; 
    for (int i=0; i<amount-1; i++) { //test all chars in the string
      test=word.charAt(i);
      if ('a'<=test && test<='z') {
        k=k+1; //increment k to check if all characters are letters
      }
    }
    if (amount==1+k) {
       value= true;
    }
    else {
      value= false; 
    }
    return value;    
  }
  
  public static void main (String[]args) {
    
    Scanner A=new Scanner(System.in);
    System.out.println("Please enter a string: ");
    String word=A.next();
    System.out.println("Please enter 0 if you would like to analyze the entire string, or 1 to analyze part of it: ");
    int choice=A.nextInt();
    boolean value=false; //initialize the return value
    while (choice!=1 && choice!=0) {
      System.out.println("Please enter 0 if you would like to analyze the entire string, or 1 to analyze part of it: ");
      choice=A.nextInt();
    }
    if (choice==0) {
      value= wholeWord ( word, A);
    }
    if (choice==1) {
      System.out.println("Please enter the number of characters you would like to analyze: ");
      int amount=A.nextInt();
      value= partWord(word, amount, A);
      
    }
   if (value==true) {
     System.out.println("Your string is a word!");
   }
   if (value==false) {
     System.out.println("Your string is not a word.");
   }
    
  }
  
}



