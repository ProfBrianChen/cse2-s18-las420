////////////////////////////
///Lindsay Slavit 2.13.18
///CSE 2 Pyramid
///Program that calculates the volume of a pyramid

import java.util.Scanner;

public class Pyramid {
  
  public static void main (String[] args) {
    
    Scanner S= new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): ");
    double L= S.nextDouble(); //assign side length variable L
    System.out.print("The height of the pyramid is (input height): ");
    double H= S.nextDouble(); //assign height variable H
    double X= Math.pow(L, 2.0); //find area of base of pyramid
    double V= X*H/3; //calculate volume of pyramid
    String.format("%.0f", V); //remove decimal places after volume
    System.out.println("The volume inside the pyramid is: "+V+ "");
  }
}