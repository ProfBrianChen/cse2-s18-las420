////////////////////////////
///Lindsay Slavit 2.13.18
///CSE 2 Convert
///Program that converts acres of land and inches of rain to cubic miles

import java.util.Scanner;

public class Convert {
  
  public static void main (String[]args) {
    
    Scanner S = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: ");
    double A = S.nextDouble();
    System.out.print("Enter the rainfall in the affected area: ");
    double I = S.nextDouble();
    double MA=0.0015625*A; //calculate the square miles given the acres
    double MR=.0000157828*I; //calculate miles from inches of rain
    double CM= MA * MR; //calculate cubic miles of rain
    System.out.println( +CM+ " cubic miles"); //print cubic miles of rain
    
    
    
  }
}