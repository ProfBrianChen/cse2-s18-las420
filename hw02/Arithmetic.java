//////////////////////
///Lindsay Slavit 2.6.18
///CSE 2 Arithmetic
///This program calculates the cost of clothes with tax

public class Arithmetic {
  
    public static void main(String[] args) {
       //Number of pairs of pants
      int numPants = 3;
      //Cost per pair of pants
      double pantsPrice = 34.98;

      //Number of sweatshirts
      int numShirts = 2;
      //Cost per shirt
      double shirtPrice = 24.99;

      //Number of belts
      int numBelts = 1;
      //cost per box of envelopes
      double beltCost = 33.99;

      //the tax rate
      double paSalesTax = 0.06;
      
      double totalCostOfPants;   //total cost of pants
      totalCostOfPants= numPants*pantsPrice;
      double totalCostOfShirts;  //total cost of shirts
      totalCostOfShirts= numShirts*shirtPrice;
      double totalCostOfBelts; //total cost of belts
      totalCostOfBelts= numBelts*beltCost;
      double salesTaxPants;
      salesTaxPants=(paSalesTax*totalCostOfPants)*100;
      salesTaxPants= (int) salesTaxPants;
      salesTaxPants=salesTaxPants/100; //sales tax on pants 
      double salesTaxShirts;
      salesTaxShirts=paSalesTax*totalCostOfShirts*100;
      salesTaxShirts= (int) salesTaxShirts;
      salesTaxShirts=salesTaxShirts/100; //sales tax on shirts
      double salesTaxBelts;
      salesTaxBelts=paSalesTax*totalCostOfBelts*100;
      salesTaxBelts= (int) salesTaxBelts;
      salesTaxBelts=salesTaxBelts/100; //sales tax on belts
      double totalCost;
      totalCost= totalCostOfPants+totalCostOfShirts+totalCostOfBelts*100/100; //cost of all items
      double totalTax;
      totalTax= salesTaxPants+salesTaxShirts+salesTaxBelts*100/100; //total tax
      double total;
      total=totalCost+totalTax; //total paid
      System.out.println("Item     Total Cost     Tax Paid\n");
      System.out.println("Pants    $"   +totalCostOfPants+"        $"   + salesTaxPants+"");
      System.out.println("Shirts   $"   +totalCostOfShirts+"         $"+  salesTaxShirts+"");
      System.out.println("Belts    $"   +totalCostOfBelts+ "         $"  + salesTaxBelts+"\n");
      System.out.println("Total Cost Before Tax $"   +totalCost+"");
      System.out.println("Total Tax $"     +totalTax+"");
      System.out.println("Total Cost $"    +total+"");
    }
}