///////////////////////////
///Lindsay Slavit 4.17.18
///CSE 2 --> Create a program 

public class DrawPoker {
	
	public static boolean fullHouse(boolean a, boolean b) {
		if (a==true && b==true) { //check for full house using pair and threeOfAKind
			return true;
		}
		else {
			return false;
		}
	}
	
	public static boolean flush(int[] hand) {
    int suitType=0; //check for flush (all cards of same suit)
    int countHearts=0;
    int countSpades=0;
    int countClubs=0;
    int countDiamonds=0;

    String suit[]=new String[hand.length];

    for(int i=0; i<hand.length; i++) {
      suitType=hand[i]/13;
      switch (suitType) {
        case 0:
          suit[i]="diamonds";
					countDiamonds+=1; //increment counter
          break;
        case 1:
          suit[i]="clubs";
					countClubs+=1; //increment counter
          break;
        case 2:
          suit[i]="hearts";
					countHearts+=1; //increment counter
          break;
        case 3:
          suit[i]="spades";   
					countSpades+=1; //increment counter
          break;
      }}
      if (countSpades==5||countHearts==5||countClubs==5||countDiamonds==5) {
				return true;
			}
			else{
				return false;
			}
  }
  
  public static boolean pair(int[] hand) {
    int remainder=0;
    int countAce=0;
    int count1=0;
    int count2=0;
    int count3=0;
    int count4=0;
    int count5=0;
    int count6=0;
    int count7=0;
    int count8=0;
    int count9=0;
    int countJack=0;
    int countQueen=0;
    int countKing=0;

    String card[]=new String[hand.length];
    
    for(int i=0; i<hand.length; i++) {
			remainder=hand[i]%13;
      switch(remainder) {
        case 0:
          card[i]="ace";
          countAce+=1; //increment counter
          break;
        case 1:
          card[i]="1";
          count1+=1; //increment counter
          break;
        case 2:
          card[i]="2";
          count2+=1; //increment counter
          break;
        case 3:
          card[i]="3";
          count3+=1; //increment counter
          break;
        case 4:
          card[i]="4";
          count4+=1; //increment counter
          break;
        case 5:
          card[i]="5";
          count5+=1; //increment counter
          break;
        case 6:
          card[i]="6";
          count6+=1; //increment counter
          break;
        case 7:
          card[i]="7";
          count7+=1; //increment counter
          break;
        case 8:
          card[i]="8";
          count8+=1; //increment counter
          break;
        case 9:
          card[i]="9";
          count9+=1; //increment counter
          break;
        case 10:
          card[i]="jack";
          countJack+=1; //increment counter
          break;
        case 11:
          card[i]="queen";
          countQueen+=1; //increment counter
          break;
        case 12:
          card[i]="king";
          countKing+=1; //increment counter
          break;
    }}
      if (countAce==2||count1==2||count2==2||count3==2||count4==2||count5==2||count6==2||count7==2||count8==2||count9==2||countJack==2||countQueen==2||countKing==2) {
				return true;
			}
			else{
				return false;
			}
  }
  
	
	public static boolean threeOfAKind(int[] hand) {
    int remainder=0;
    int countAce=0;
    int count1=0;
    int count2=0;
    int count3=0;
    int count4=0;
    int count5=0;
    int count6=0;
    int count7=0;
    int count8=0;
    int count9=0;
    int countJack=0;
    int countQueen=0;
    int countKing=0;

    String card[]=new String[hand.length];
    
    for(int i=0; i<hand.length; i++) {
			remainder=hand[i]%13;
      switch(remainder) {
        case 0:
          card[i]="ace";
          countAce+=1; //increment counter
          break;
        case 1:
          card[i]="1";
          count1+=1; //increment counter
          break;
        case 2:
          card[i]="2";
          count2+=1; //increment counter
          break;
        case 3:
          card[i]="3";
          count3+=1; //increment counter
          break;
        case 4:
          card[i]="4";
          count4+=1; //increment counter
          break;
        case 5:
          card[i]="5";
          count5+=1; //increment counter
          break;
        case 6:
          card[i]="6";
          count6+=1; //increment counter
          break;
        case 7:
          card[i]="7";
          count7+=1; //increment counter
          break;
        case 8:
          card[i]="8";
          count8+=1; //increment counter
          break;
        case 9:
          card[i]="9";
          count9+=1; //increment counter
          break;
        case 10:
          card[i]="jack";
          countJack+=1; //increment counter
          break;
        case 11:
          card[i]="queen";
          countQueen+=1; //increment counter
          break;
        case 12:
          card[i]="king";
          countKing+=1; //increment counter
          break;
    }}
      if (countAce==3||count1==3||count2==3||count3==3||count4==3||count5==3||count6==3||count7==3||count8==3||count9==3||countJack==3||countQueen==3||countKing==3) {
				return true;
			}
			else{
				return false;
			}
  }
  public static int[] shuffle(int[] array) {
    for (int i=0; i<array.length; i++) { 
      int target = (int) (array.length * Math.random() ); //create random array
	    int temp = array[target]; //create temporary array
    	array[target] = array[i]; 
	    array[i] = temp; //switch values
    }
    return array;
  }
  
  
  public static void main (String[]args) {
    int[] deck=new int[52];
    int remainder;
    int suitType;
    String[] suit=new String[52];
    String[] card=new String[52];
    for (int i=0; i<deck.length; i++) {
      deck[i]=i;
      remainder=i%13;
      suitType=i/13;
      switch (suitType) {
        case 0:
          suit[i]="diamonds";
          break;
        case 1:
          suit[i]="clubs";
          break;
        case 2:
          suit[i]="hearts";
          break;
        case 3:
          suit[i]="spades";    
          break;
      }
      switch (remainder) {
        case 0:
          card[i]="ace";
          break;
        case 1:
          card[i]="1";
          break;
        case 2:
          card[i]="2";
          break;
        case 3:
          card[i]="3";
          break;
        case 4:
          card[i]="4";
          break;
        case 5:
          card[i]="5";
          break;
        case 6:
          card[i]="6";
          break;
        case 7:
          card[i]="7";
          break;
        case 8:
          card[i]="8";
          break;
        case 9:
          card[i]="9";
          break;
        case 10:
          card[i]="jack";
          break;
        case 11:
          card[i]="queen";
          break;
        case 12:
          card[i]="king";
          break;
    }
  }
    int[] drawDeck=shuffle(deck);
    int[] hand1=new int[5];
    int[] hand2=new int[5];
    hand1[0]=drawDeck[0];
    hand2[0]=drawDeck[1];
    hand1[1]=drawDeck[2];
    hand2[1]=drawDeck[3];
    hand1[2]=drawDeck[4];
    hand2[2]=drawDeck[5];
    hand1[3]=drawDeck[6];
    hand2[3]=drawDeck[7];
    hand1[4]=drawDeck[8];
    hand2[4]=drawDeck[9];
		
		System.out.print("Player 1 cards: ");
    for (int i=0; i<5; i++){
			remainder=hand1[i]%13;
      suitType=hand1[i]/13;
			switch (suitType) { //declare suits and card values
        case 0:
          suit[i]="diamonds";
          break;
        case 1:
          suit[i]="clubs";
          break;
        case 2:
          suit[i]="hearts";
          break;
        case 3:
          suit[i]="spades";    
          break;
      }
      switch (remainder) {
        case 0:
          card[i]="ace";
          break;
        case 1:
          card[i]="1";
          break;
        case 2:
          card[i]="2";
          break;
        case 3:
          card[i]="3";
          break;
        case 4:
          card[i]="4";
          break;
        case 5:
          card[i]="5";
          break;
        case 6:
          card[i]="6";
          break;
        case 7:
          card[i]="7";
          break;
        case 8:
          card[i]="8";
          break;
        case 9:
          card[i]="9";
          break;
        case 10:
          card[i]="jack";
          break;
        case 11:
          card[i]="queen";
          break;
        case 12:
          card[i]="king";
          break;
    }
      System.out.print(card[i]+ " of "+suit[i]+", "); 
		}
    System.out.println();
		System.out.print("Player 2 cards: ");
    for (int i=0; i<5; i++) {
			remainder=hand2[i]%13;
      suitType=hand2[i]/13;
			switch (suitType) {
        case 0:
          suit[i]="diamonds";
          break;
        case 1:
          suit[i]="clubs";
          break;
        case 2:
          suit[i]="hearts";
          break;
        case 3:
          suit[i]="spades";    
          break;
      }
      switch (remainder) {
        case 0:
          card[i]="ace";
          break;
        case 1:
          card[i]="1";
          break;
        case 2:
          card[i]="2";
          break;
        case 3:
          card[i]="3";
          break;
        case 4:
          card[i]="4";
          break;
        case 5:
          card[i]="5";
          break;
        case 6:
          card[i]="6";
          break;
        case 7:
          card[i]="7";
          break;
        case 8:
          card[i]="8";
          break;
        case 9:
          card[i]="9";
          break;
        case 10:
          card[i]="jack";
          break;
        case 11:
          card[i]="queen";
          break;
        case 12:
          card[i]="king";
          break;
    }
      System.out.print(card[i]+ " of "+suit[i]+ ", "); 
		}
		
		System.out.println();
		Boolean hand1Pair=pair(hand1); //run pair method hand 1
		Boolean hand2Pair=pair(hand2); //run pair method hand 2
		Boolean hand1Three=threeOfAKind(hand1); //run threeOfAKind method hand 1
		Boolean hand2Three=threeOfAKind(hand2); //run threeOfAKind method hand 2
		Boolean hand1Flush=flush(hand1); //run flush method hand 1
		Boolean hand2Flush=flush(hand2);//run flush method hand 2
		Boolean hand1FullHouse=fullHouse(hand1Pair,hand1Three); //run fullHouse method hand 1
		Boolean hand2FullHouse=fullHouse(hand2Pair,hand2Three); //run fullHouse method hand 2
    int score1=0; //initialize scoring
		int score2=0; //initialize scoring
		if(hand1Pair==true) {
			score1+=2;
		}
		if(hand2Pair==true) {
			score2+=2;
		}
		if(hand1Three==true) {
			score1+=3;
		}
		if(hand2Three==true) {
			score2+=3;
		}
		if(hand1Flush==true) {
			score1+=4;
		}
    if(hand2Flush==true) {
			score2+=4;
		}
   if(hand1FullHouse==true) {
			score1+=5;
		}
    if(hand2FullHouse==true) {
			score2+=4;
		}
		/*System.out.println("Player 1 score is: "+score1);
		System.out.println("Player 2 score is: " +score2);*/
		if(score1>score2) {
			System.out.println("Player 1 wins!"); 
		}
		int highest1=hand1[0] % 13;
		int highest2=hand2[0]  %13;
		if (score1==score2) {
			for(int i=1;i<5;i++) {
				if (hand1[i] % 13>hand1[i-1] %13) { //figure out highest card
					highest1=hand1[i] %13;
				}
				if (hand2[i] % 13>hand2[i-1] %13) {
					highest2=hand2[i] %13;
				}
			}
			if (highest1>highest2) {
				System.out.println("Player 1 wins by highest card!");
			}
			if (highest1<highest2) {
				System.out.println("Player 2 wins by highest card!");
			}
			if(highest1==highest2){
				System.out.println("Tie!");
			}
			
		}
		if(score2>score1) {
			System.out.println("Player 2 wins!");
		}
}
}

