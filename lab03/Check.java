//////////////////////
///Lindsay Slavit 2.9.18
///CSE 2 Check
///Program that calculates check per person

import java.util.Scanner;

public class Check {
  
  public static void main (String[]args) {
    
    Scanner A = new Scanner( System.in );
    System.out.print("Enter the original cost of the check in the form xx.xx: $"); //prompt user to enter bill
    double checkCost = A.nextDouble(); //assign input as check cost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //prompt user to enter tip percentage
    double tipPercent = A.nextDouble(); //assign input as tip percent
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //prompt user to enter number of people
    int numPeople = A.nextInt();// assign input as number of people
    double totalCost;
    double costPerPerson;
    int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //print check


    
    
    
  }
}
