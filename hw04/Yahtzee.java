///////////////////////////
///Lindsay Slavit 2.20.18
///CSE 2 --> Create a Yahtzee! program 

import java.util.Scanner;

public class Yahtzee {
    
    public static void main (String [] args) {
      
      Scanner S = new Scanner (System.in);
      System.out.print("If you would like to roll press 1, if you already rolled press a different number ");
      int ans= S.nextInt();
      int roll=0;
      int roll1=0;
      int roll2=0;
      int roll3=0;
      int roll4=0;
      int roll5=0; 
      int aces=0;
      int twos=0;
      int threes=0;
      int fours=0;
      int fives=0;
      int sixes=0;
      int threeOfAKind=0;
      int fourOfAKind=0;
      int yahtzee=0;
      int smallStraight=0;
      int largeStraight=0;
      int chance=0;
      int fullHouse=0;
      int upperSectionTotal=0;
      
      if (ans==1) {
        roll1 = (int )(Math.random() * 6 + 1); //generate a random number between 1 and (inclusive)
        roll2 = (int )(Math.random() * 6 + 1); //generate a random number between 1 and (inclusive)
        roll3 = (int )(Math.random() * 6 + 1); //generate a random number between 1 and (inclusive)
        roll4 = (int )(Math.random() * 6 + 1); //generate a random number between 1 and (inclusive)
        roll5 = (int )(Math.random() * 6 + 1); //generate a random number between 1 and (inclusive)
      }
      else {
        System.out.print("Please type in your roll"); //prompt user to enter their roll
        roll= S.nextInt();
        int length = String.valueOf(roll).length();
        if (length!= 5) {
           System.out.print ("Error: incorrect roll length");
           return;
        }
        
        roll5= roll % 10; //get fifth digit of roll
        roll=roll/10; //increment roll
        roll4= roll % 10; //get fourth digit of roll
        roll=roll/10; //increment roll
        roll3= roll % 10; // get third digit of roll
        roll=roll/10; //increment roll
        roll2= roll % 10; // get second digit of roll
        roll1=roll/10; //increment roll
        if (roll1<0 || roll1>6) {
          System.out.print ("Error: roll outside of range");
          return;
        }
        if (roll2<0 || roll2>6) {
          System.out.print ("Error: roll outside of range");
          return;
        }
        if (roll3<0 || roll3>6) {
          System.out.print ("Error: roll outside of range");
          return;
        }
        if (roll4<0 || roll4>6) {
          System.out.print ("Error: roll outside of range");
          return;
        }
        if (roll5<0 || roll5>6) {
          System.out.print ("Error: roll outside of range");
          return;
        }
      }
      int upperSection= roll1+roll2+roll3+roll4+roll5; //compute upper section total
      if (upperSection>63) {
        upperSectionTotal=upperSection+35; //add bonus
      }
      else {
        upperSectionTotal=upperSection; 
        
      }
      if (roll1==1) 
        aces=aces+1; //increment aces
      if (roll1==2)
        twos=twos+1; //increment twos
      if (roll1==3)
        threes=threes+1; //increment threes
      if (roll1==4) 
        fours=fours+1; //increment fours
      if (roll1==5)
        fives=fives+1; //increment fives
      if (roll1==6)
        sixes=sixes+1; //increment sixes
      if (roll2==1) 
        aces=aces+1; //increment aces
      if (roll2==2)
        twos=twos+1; //increment twos
      if (roll2==3)
        threes=threes+1; //increment threes
      if (roll2==4) 
        fours=fours+1; //increment fours
      if (roll2==5)
        fives=fives+1; //increment fives
      if (roll2==6)
        sixes=sixes+1; //increment sixes
      if (roll3==1) 
        aces=aces+1; //increment aces
      if (roll3==2)
        twos=twos+1; //increment twos
      if (roll3==3)
        threes=threes+1; //increment threes
      if (roll3==4) 
        fours=fours+1; //increment fours
      if (roll3==5)
        fives=fives+1; //increment fives
      if (roll3==6)
        sixes=sixes+1; //increment sixes
      if (roll4==1) 
        aces=aces+1; //increment aces
      if (roll4==2)
        twos=twos+1; //increment twos
      if (roll4==3)
        threes=threes+1; //increment threes
      if (roll4==4) 
        fours=fours+1; //increment fours
      if (roll4==5)
        fives=fives+1; //increment fives
      if (roll4==6)
        sixes=sixes+1; //increment sixes
      if (roll5==1) 
        aces=aces+1; //increment aces
      if (roll5==2)
        twos=twos+1; //increment twos
      if (roll5==3)
        threes=threes+1; //increment threes
      if (roll5==4)  
        fours=fours+1; //increment fours
      if (roll5==5)
        fives=fives+1; //increment fives
      if (roll5==6)
        sixes=sixes+1; //increment sixes
    switch (aces) {
      case 3:
        threeOfAKind=3;
        break;
      case 4:
        fourOfAKind=4;
        break;
      case 5:
        yahtzee=50;
        break;
    }
     switch (twos) { 
      case 3:
        threeOfAKind=6;
        break;
      case 4:
        fourOfAKind=8;
        break;
      case 5:
        yahtzee=50;
        break;
     }
     switch (threes) {
      case 3:
        threeOfAKind=9;
        break;
      case 4:
        fourOfAKind=12;
        break;
      case 5:
        yahtzee=50;
        break;
     } 
     switch (fours) {
      case 3:
        threeOfAKind=12;
        break;
      case 4:
        fourOfAKind=16;
        break;
      case 5:
        yahtzee=50;
        break;
     }
     switch (fives) {
      case 3:
        threeOfAKind=15;
        break;
      case 4:
        fourOfAKind=20;
        break;
      case 5:
        yahtzee=50;
        break;
     }
     switch (sixes) {
      case 3:
        threeOfAKind=18;
        break;
      case 4:
        fourOfAKind=24;
        break;
      case 5:
        yahtzee=50;
        break;
     }

     if (aces==2 && twos==3)
       fullHouse=25;
     if (aces==2 && threes==3)
       fullHouse=25; 
     if (aces==2 && fours==3)
       fullHouse=25;    
     if (aces==2 && fives==3)
       fullHouse=25; 
     if (aces==2 && sixes==3)
       fullHouse=25; 
     if (twos==2 && aces==3)
       fullHouse=25;  
     if (twos==2 && threes==3)
       fullHouse=25; 
     if (twos==2 && fours==3)
       fullHouse=25;
     if (twos==2 && fives==3)
       fullHouse=25;
     if (twos==2 && sixes==3)
       fullHouse=25;
     if (threes==2 && aces==3)
       fullHouse=25; 
     if (threes==2 && twos==3)
       fullHouse=25;
     if (threes==2 && fours==3)
       fullHouse=25;
     if (threes==2 && fives==3)
       fullHouse=25;
     if (threes==2 && sixes==3)
       fullHouse=25;
     if (fours==2 && aces==3)
       fullHouse=25;
     if (fours==2 && twos==3)
       fullHouse=25;
     if (fours==2 && threes==3)
       fullHouse=25;
     if (fours==2 && fives==3)
       fullHouse=25;
     if (fours==2 && sixes==3)
       fullHouse=25;
     if (fives==2 && aces==3)
       fullHouse=25;
     if (fives==2 && twos==3)
       fullHouse=25;
     if (fives==2 && threes==3)
       fullHouse=25;
     if (fives==2 && fours==3)
       fullHouse=25;
     if (fives==2 && sixes==3)
       fullHouse=25;
     if (sixes==2 && aces==3)
       fullHouse=25;
     if (sixes==2 && twos==3)
       fullHouse=25;
     if (sixes==2 && threes==3)
       fullHouse=25;
     if (sixes==2 && fours==3)
       fullHouse=25;
     if (sixes==2 && fives==3)
       fullHouse=25;
     if (aces==1 && twos==1 && threes==1 && fours==1 && fives!=1) {
       smallStraight=30;
     }
     if (aces!=1 && twos==1 && threes==1 && fours==1 && fives==1 && sixes!=1) {
       smallStraight=30;
     }
     if (twos!=1 && threes==1 && fours==1 && fives==1 && sixes==1) {
       smallStraight=30;
     }
     if (aces==1 && twos==1 && threes==1 && fours==1 && fives==1 && sixes!=1) {
       largeStraight=40;
     }
     if (aces!=1 && twos==1 && threes==1 && fours==1 && fives==1 && sixes==1) {
       largeStraight=40;
     }
     chance=roll1+roll2+roll3+roll4+roll5; //sum the die for chance 
     int lowerTotal=threeOfAKind+fourOfAKind+yahtzee+fullHouse+smallStraight+largeStraight+chance; //calculate lower section Total
     int grandTotal= lowerTotal+ upperSectionTotal; //calculate grand total
     System.out.println("You rolled: ");
     System.out.println(roll1+ " " +roll2+ " " +roll3+ " " +roll4+ " " +roll5);
     System.out.println("Your initial upper section total is: " +upperSection);
     System.out.println("Your upper section total with the bonus is: " +upperSectionTotal);
     System.out.println("Your lower section total is: " +lowerTotal);
     System.out.println("Your grand total is: " +grandTotal);
        
  
    }
}