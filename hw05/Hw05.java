/////////////////////////// n
///Lindsay Slavit 3.2.18
///CSE 2 --> Create a program that will print a twist on the screen

import java.util.Scanner;

public class Hw05 {
  
  public static void main (String[] args) {
    
    Scanner A= new Scanner (System.in);
    System.out.println("Please input the course number: "); //prompt user to input course number
    boolean flag = A.hasNextInt();
    int courseNum; 
    while (flag==false) {
      String junkWord = A.next(); 
      flag= A.hasNextInt(); //check if the new input is an int
      System.out.println("Error- Please enter an integer value: "); //make sure input is integer
      courseNum= A.nextInt(); //assign input to variable length
    }
    courseNum= A.nextInt();
    System.out.println("Please input the department name: "); //prompt user to input department
    boolean flag1 = A.hasNext();
    String department= A.next();
    while (flag1==false) {
      String junkWord = A.next();    
      flag1= A.hasNext(); //check if the new input is an int
      System.out.println("Error- Please enter a word: "); //make sure input is a string
      department= A.next(); //assign input to variable length
    }
    System.out.println("Please input the number of times the class meets per week: "); //prompt user to input meetings a week 
    boolean flag2 = A.hasNextInt();
    int timesPerWeek= A.nextInt();
    while (flag2==false) {
      String junkWord = A.next();  
      flag2= A.hasNextInt(); //check if the new input is an int
      System.out.print("Error- Please enter an integer: "); //make sure input is integer
      timesPerWeek= A.nextInt(); //assign input to variable length
    }
    System.out.println("Please input the time the class starts: "); //prompt user to input meetings a week 
    System.out.println("   Enter the hour it starts at: "); 
    boolean flag3 = A.hasNextInt();
    int hour=A.nextInt();
    while (flag3==false) {
      String junkWord = A.next();
      flag3= A.hasNextInt(); //check if the new input is an int
      System.out.println("Error- Please enter an integer: "); //make sure input is integer
      hour= A.nextInt(); //assign input to variable length
    }
    System.out.println("   Enter the minutes it starts at: ");
    boolean flag4= A.hasNextInt();
    int minutes=A.nextInt(); 
    while (flag4==false) {
      String junkWord = A.next();
      flag4= A.hasNextInt(); //check if the new input is an int
      System.out.println("Error- Please enter an integer: "); //make sure input is integer
      minutes= A.nextInt(); //assign input to variable length
    } 
    System.out.println("Enter the instructor's name: ");
    boolean flag5= A.hasNext();
    String instructorName=A.next(); 
    while (flag5==false) {
      String junkWord = A.next();    
      flag5= A.hasNext(); //check if the new input is an int
      System.out.println("Error- Please enter a word: "); //make sure input is integer
      instructorName= A.next(); //assign input to variable length
    } 
    System.out.println("Enter the number of students in the class: ");
    boolean flag6= A.hasNextInt();
    int studentNum=A.nextInt(); 
    while (flag6==false) {
      String junkWord = A.next();  
      flag4= A.hasNextInt(); //check if the new input is an int
      System.out.println("Error- Please enter an integer: "); //make sure input is integer
      studentNum= A.nextInt(); //assign input to variable length
    } 
    
    
    System.out.println("\nYour course number is " +courseNum);
    System.out.println("The department is " +department);
    System.out.println("Your course meets " +timesPerWeek+ " times per week");
    System.out.println("Your class starts at " +hour+ ":" +minutes);
    System.out.println("Your instructor's name is " +instructorName);
    System.out.println("There are " +studentNum+ " students in the class");
    
  }
}


