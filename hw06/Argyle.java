///////////////////////////
///Lindsay Slavit 3.20.18
///CSE 2 --> Create a program that will print an Argyle pattern

import java.util.Scanner;

public class Argyle {
  
  public static void main (String[] args) {
    
    Scanner A = new Scanner(System.in);
    System.out.print("Please enter a positive integer for the width of viewing window in characters: "); //prompt user for integer input
    boolean flag= A.hasNextInt();
    int windowWidth= A.nextInt(); //assign input to variable dimension
    System.out.print("Please enter a positive integer for the height of viewing window in characters: "); //prompt user for integer input
    boolean flag1=A.hasNextInt();
    int windowHeight= A.nextInt();
    System.out.print("Please enter a positive integer for the width of the argyle diamonds: "); //prompt user for integer input
    int diamondWidth=A.nextInt();
    System.out.print("Please enter a positive odd integer for the width of the argyle center stripe: "); //prompt user for integer input
    int stripeWidth= A.nextInt();
    while (stripeWidth % 2 == 0 || stripeWidth>diamondWidth/2) {
      System.out.print("Please enter a smaller positive odd integer for the width of the argyle center stripe:");
      stripeWidth= A.nextInt(); 
    }
    System.out.print("Please enter a first character for the pattern fill: "); //prompt user for integer input
    String fill1=A.next();
    char result1 = fill1.charAt(0);
    System.out.print("Please enter a second character for the pattern fill: "); //prompt user for integer input
    String fill2=A.next();
    char result2 = fill2.charAt(0);
    System.out.print("Please enter a third character for the pattern fill: "); //prompt user for integer input
    String fill3=A.next();
    char result3 = fill3.charAt(0);
    int timesAcross = windowWidth/diamondWidth; //calculate times loop has to run pattern horizontally
    int timesDown = windowHeight/diamondWidth; //calculate times loop has to run vertically
    for (int k=0; k<=timesDown; k++) {
    for(int i=0; i<=diamondWidth; i++) {
      for (int z=0; z<=timesAcross; z++) {
      for(int j=0; j<=diamondWidth; j++) {
        if(j==i) { //use for loop and cap this at stripeWitth
          System.out.print(result3); //print spaces in x
        }
        if(j>=diamondWidth-i) {
          System.out.print(result2);
        }
          
        else {
          System.out.print(result1);
        }    
        }
      System.out.println();
    }
    }
    }
    
    
    
    
  }
}
