///////////////////////////////////////////////////////////
///Lindsay Slavit 3.27.18
///CSE 2 --> Create a program that will analyze all characters in a string

import java.util.Random;
import java.util.Scanner;

public class lab07 {
  
  public static String adjective(Random randomGenerator) {
    int randomInt = randomGenerator.nextInt(10); //generate random int to assign to string
    String adj1="a";
    switch (randomInt) {
      case 0:
        adj1="quick";
        break;
      case 1:
        adj1="lazy";
        break;
      case 2:
        adj1="old";
        break;
      case 3:
        adj1="new";
        break;
      case 4:
        adj1="tired";
        break;
      case 5:
        adj1="happy";
        break;
      case 6:
        adj1="pretty";
        break;
      case 7:
        adj1="stupid";
        break;
      case 8:
        adj1="red";
        break;
      case 9:
        adj1="cold";
        break;
    }
   return adj1;
  }
  
  public static String nounSubject(Random randomGenerator) {
    int randomInt = randomGenerator.nextInt(10); //generate random int to assign to string
    String nounSub="a";
    switch (randomInt) {
      case 0:
        nounSub="fox";
        break;
      case 1:
        nounSub="girl";
        break;
      case 2:
        nounSub="Professor Chen";
        break;
      case 3:
        nounSub="boy";
        break;
      case 4:
        nounSub="Lindsay";
        break;
      case 5:
        nounSub="dog";
        break;
      case 6:
        nounSub="brother";
        break;
      case 7:
        nounSub="it";
        break;
      case 8:
        nounSub="mom";
        break;
      case 9:
        nounSub="dad";
        break;
    }
   return nounSub;
    
  }
  
  public static String verb(Random randomGenerator) {
    int randomInt = randomGenerator.nextInt(10); //generate random int to assign to string
    String action="a";
    switch (randomInt) {
      case 0:
        action="went";
        break;
      case 1:
        action="ran";
        break;
      case 2:
        action="threw";
        break;
      case 3:
        action="jumped";
        break;
      case 4:
        action="walked";
        break;
      case 5:
        action="started";
        break;
      case 6:
        action="finished";
        break;
      case 7:
        action="called";
        break;
      case 8:
        action="hid";
        break;
      case 9:
        action="took";
        break;
    }
   return action;
    
  }
  
  public static String nounObject(Random randomGenerator) {
    int randomInt = randomGenerator.nextInt(10); //generate random int to assign to string
    String nounOb="a";
    switch (randomInt) {
      case 0:
        nounOb="him";
        break;
      case 1:
        nounOb="her";
        break;
      case 2:
        nounOb="laptop";
        break;
      case 3:
        nounOb="cup";
        break;
      case 4:
        nounOb="classroom";
        break;
      case 5:
        nounOb="refridgerator";
        break;
      case 6:
        nounOb="stove";
        break;
      case 7:
        nounOb="cracker";
        break;
      case 8:
        nounOb="snack";
        break;
      case 9:
        nounOb="juice";
        break;
    }
   return nounOb;
    
  }
  
  public static String thesisGenerator(Random randomGenerator) {
   Scanner A= new Scanner(System.in);
   String adj=adjective(randomGenerator);
   String adj1=adjective(randomGenerator);
   String adj2=adjective(randomGenerator);
   String sub=nounSubject(randomGenerator);
   String action=verb(randomGenerator);
   String ob=nounObject(randomGenerator);
   System.out.println("The " +adj+" "+adj1+" "+sub+" "+action+" the "+adj2+" "+ob+".");
   return sub;
  }
  
  public static String actionGenerator(Random randomGenerator, String sub) {
    String ob=nounObject(randomGenerator);
    String ob1=nounObject(randomGenerator);
    String ob2=nounObject(randomGenerator);
    String action=verb(randomGenerator);
    String adj=adjective(randomGenerator);
    String action1=verb(randomGenerator);
    System.out.println("The " +sub+ " "+action1+ " "+ob+ " to " +action+ " " +ob1+ " at the " +adj+ " " +ob2+".");
    return adj;
  }
  
  public static String conclusionGenerator(Random randomGenerator, String sub) {
    String ob=nounObject(randomGenerator);
    String action=verb(randomGenerator);
    System.out.println("That " +sub+ " " +action+ " her " +ob+".");
    return action;
  }
  
  public static void main (String[]args) {
    Random randomGenerator=new Random();
    Scanner A=new Scanner(System.in);
    String sub="a";
    String action="a";
    int choice;
    do {
    System.out.println("Please enter any number for a paragraph, enter 0 to quit: ");
    int sentenceNumber=randomGenerator.nextInt(10);
    choice=A.nextInt();
     if (choice==0) {
       return;
    }
    else {
      sub=thesisGenerator(randomGenerator);
      for(int i=0; i<sentenceNumber; i++) {
        action=actionGenerator(randomGenerator, sub);
        
      }
      String conclusion=conclusionGenerator(randomGenerator, sub);
     
 
     }
    
   } while (choice!=0);
   
  }
  
  
}


