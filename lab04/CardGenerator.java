///////////////////////////
///Lindsay Slavit 2.16.18
///CSE 2 --> Create a program that generates random cards and then assigns a suite and number

public class CardGenerator {
  
  public static void main (String[] args) {
    
    int rand = (int )(Math.random() * 52 + 1); //generate a random number between 1 and 52 (inclusive)
    String suit= "abc";
    String identity= "abc";
    int rem = rand % 13; // find the remainder when the variable is divided by 13
    if (rand <=13 && rand>=1) {
        suit= "Diamonds"; //assign suit diamonds
        switch (rand) {
          case 1:
              identity= "Ace"; //assign the identity
          break;
          case 2:
              identity= "2";
          break;
          case 3:
              identity= "3";
          break;
          case 4:
              identity= "4";
          break;
          case 5:
              identity= "5";
          break;
          case 6:
              identity= "6";
          break;
          case 7:
              identity= "7";
          break;
          case 8:
              identity= "8";
          break;
          case 9:
              identity= "9";
          break;
          case 10:
              identity= "10";
          break;
          case 11: 
              identity = "Jack";
          break;    
          case 12:
              identity = "Queen";
          break;
          case 13:
             identity = "King";
          break;
        }
    }
    if (rand <=26 && rand>=14) {
        suit= "Clubs"; //assign suit clubs
    }
    if (rand <=39 && rand>=27) {
        suit= "Hearts"; //assign suit hearts
    }
    if (rand <=52 && rand>=40) {
        suit= "Spades"; //assign suit spades
    }
    if (rand>13) {
     switch (rem) {
          case 1:
              identity= "Ace";  //assign the identity
          break;
          case 2:
              identity= "2";
          break;
          case 3:
              identity= "3";
          break;
          case 4:
              identity= "4";
          break;
          case 5:
              identity= "5";
          break;
          case 6:
              identity= "6";
          break;
          case 7:
              identity= "7";
          break;
          case 8:
              identity= "8";
          break;
          case 9:
              identity= "9";
          break;
          case 10:
              identity= "10";
          case 11: 
              identity = "Jack";
          break;    
          case 12:
              identity = "Queen";
          break;
          case 0:
             identity = "King";
          break;
        }
    }
    System.out.println("You picked the " +identity+ " of " +suit+ "");
  }
}