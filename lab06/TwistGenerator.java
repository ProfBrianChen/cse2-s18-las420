///////////////////////////
///Lindsay Slavit 3.2.18
///CSE 2 --> Create a program that will print a twist on the screen

import java.util.Scanner;

public class TwistGenerator {
  
  public static void main (String[] args) {
    
    Scanner A = new Scanner(System.in);
    System.out.print("Please enter an integer value for the length: "); //prompt user for integer input
    boolean flag= A.hasNextInt();
    int length= A.nextInt(); //assign input to variable length
    while (flag==false) {
      String junkWord = A.next();      
      flag= A.hasNextInt();
      System.out.print("Please enter a positive integer value: "); //make sure input is integer
      length= A.nextInt(); //assign input to variable length
    }
    while (length<0) {
      System.out.print("Please enter a positive integer value: ");
      length= A.nextInt(); //assign input to variable length
    }
    int rem= length % 3; //find out remainder
    int times=length/3; //find out how many times the print statement should run
    switch (rem) {
      case 0:
        for (int countln1=1; countln1<=times; countln1++) {
         System.out.print("\\ /");
        
        }
        System.out.print("\n");
        for (int countln2=1; countln2<=times; countln2++) {
         System.out.print(" X ");
         
        }
        System.out.print("\n");
        for (int countln3=1; countln3<=times; countln3++) {
         System.out.print("/ \\"); 
         
        }
        break;
      case 1:  
        for (int countln1=1; countln1<=times; countln1++) {
         System.out.print("\\ /");
          
        }
        System.out.println("\\");
        for (int countln2=1; countln2<=times; countln2++) {
         System.out.print(" X ");
         
        }
        System.out.print("\n");
        for (int countln3=1; countln3<=times; countln3++) {
         System.out.print("/ \\"); 
       
        }  
        System.out.println("/");   
        break;
       case 2:  
        for (int countln1=1; countln1<=times; countln1++) {
         System.out.print("\\ /");
          
        }
        System.out.println("\\ ");
        for (int countln2=1; countln2<=times; countln2++) {
         System.out.print(" X ");
         
        }
        System.out.println(" X");
        for (int countln3=1; countln3<=times; countln3++) {
         System.out.print("/ \\"); 
       
        }  
         System.out.println("/ ");
        break;
        
     }
    
  }
  
}