///////////////////////////
///Lindsay Slavit 3.9.18
///CSE 2 --> Create a program that will print an encoded x within stars on the screen

import java.util.Scanner;

public class encrypted_x {
  
  public static void main (String[] args) {
    
    Scanner A = new Scanner(System.in);
    System.out.print("Please enter an integer value for the box dimension: "); //prompt user for integer input
    boolean flag= A.hasNextInt();
    int dimension= A.nextInt(); //assign input to variable dimension
    while (flag==false) {
      String junkWord = A.next();      
      flag= A.hasNextInt();
      System.out.print("Please enter a positive integer value: "); //make sure input is integer
      dimension= A.nextInt(); //assign input to variable dimension
    }
    while (dimension<0 || dimension>100) {
      System.out.print("Please enter a positive integer value: ");
      dimension= A.nextInt(); //assign input to variable dimension
    }
    for(int i=0; i<=dimension; i++) {
      for(int j=0; j<=dimension; j++) {
        if(j==i || j==dimension-i) {
          System.out.print(" "); //print spaces in x
        }
        else {
          System.out.print("*");
        }    
        }
      System.out.println();
    }

  }
}

