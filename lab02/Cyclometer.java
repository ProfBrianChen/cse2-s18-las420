//////////////////////
///Lindsay Slavit 2.4.18
///CSE 2 Cyclometer
///Program that calculates time and rotation of front wheel for bike trips

public class Cyclometer {
  
  public static void main (String[]args) {
    int secsTrip1=480;  //time trip 1
    int secsTrip2=3220;  //time trip 2
		int countsTrip1=1561;  //tire rotations trip 1
		int countsTrip2=9037;  //tire rotations trip 2

    double wheelDiameter=27.0,  //diameter of wheel
  	PI=3.14159, //value of pi
  	feetPerMile=5280,  //feet in a mile
  	inchesPerFoot=12,   //inches in a food
  	secondsPerMinute=60;  //seconds in a munute
	  double distanceTrip1, distanceTrip2,totalDistance;  //initialize variables for trip distances
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+ " minutes and had "+countsTrip1+" counts.");//print trip 1
	  System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");//print trip 2
    
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
  	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
  	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Calculate trip 2 distance
  	totalDistance=distanceTrip1+distanceTrip2; //calculate total distance
    System.out.println("Trip 1 was "+distanceTrip1+" miles");//print trip 1 distance
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");//print trip 2 distance
    System.out.println("The total distance was "+totalDistance+" miles");//print total distance
  }
}